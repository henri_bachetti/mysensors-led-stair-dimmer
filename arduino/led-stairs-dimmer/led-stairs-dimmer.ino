
#define LOG_DOMAIN    LOG_MAIN
#include "debug.h"

#define LDR
// light level in LUX
#define LIGHT_MAX_LEVEL       5.0
// dimmer clock in microseconds
#define DIMMER_CLOCK          2000
// dimmer ON time in milliseconds
#define DIMMER_ON_TIME        15000
// MYSENSORS option (NRF24L01)
//#define MYSENSORS

#include <Wire.h>
#include <Bounce2.h>

#include "encoder.h"
#include "dimmer.h"

#define SX1509_ADDRESS        0x3E
#define KNOB_ENC_PIN_1        2
#define KNOB_ENC_PIN_2        3
#define KNOB_BUTTON_PIN       4
#define DOWN_PIR_PIN          5
#define UP_PIR_PIN            6
#define LUMINOSITY_PIN        0

#define EEPROM_DIM_LEVEL      1

#ifdef MYSENSORS
#define MY_RADIO_NRF24
//#define MY_RADIO_RFM69
#define MY_RF24_CE_PIN    7
#define MY_RF24_CS_PIN    8
#define MY_DEBUG
#include <SPI.h>
#include <MySensors.h>

#define CHILD_ID_LUMINOSITY   0
#define CHILD_ID_LIGHT        1
#define CHILD_ID_MOTION_UP    2
#define CHILD_ID_MOTION_DOWN  3
#endif

#define SN "LED stairs dimmer"
#define SV "1.0"

bool changedByKnob = false;
bool sendDimValue = false;
char convBuffer[10];

#ifdef MYSENSORS
MyMessage dimmerMsg(CHILD_ID_LIGHT, V_DIMMER);
MyMessage statusMsg(CHILD_ID_LIGHT, V_STATUS);
MyMessage motionUpMsg(CHILD_ID_MOTION_UP, V_TRIPPED);
MyMessage motionDownMsg(CHILD_ID_MOTION_DOWN, V_TRIPPED);
MyMessage luminosityMsg(CHILD_ID_LUMINOSITY, V_LIGHT_LEVEL);
#endif

Encoder knob(KNOB_ENC_PIN_1, KNOB_ENC_PIN_2, KNOB_BUTTON_PIN);
Bounce debouncer = Bounce();

DimmingLed leds[] = {DimmingLed(8), DimmingLed(9), DimmingLed(10), DimmingLed(11),
                     DimmingLed(12), DimmingLed(13), DimmingLed(14), DimmingLed(15),
                     DimmingLed(0), DimmingLed(1), DimmingLed(2), DimmingLed(3),
                     DimmingLed(4), DimmingLed(5), DimmingLed(6), DimmingLed(7)
                    };
int _nLeds = sizeof(leds) / sizeof(leds[0]);

// leds: DimmingLed array
// n: 16 LEDs
// clock: DIMMER_CLOCK
// onTrigger: 10%
// offTrigger: 3%
// brightness: 50%
Dimmer dimmer(leds, _nLeds, DIMMER_CLOCK, 10, 3, 50);

Bounce downDebouncer = Bounce();
Bounce upDebouncer = Bounce();

#define VREF 3.3
#define RREF 10000.0
#define LUX_CALC_SCALAR 14500000
#define LUX_CALC_EXPONENT -1.405
#define NSAMPLE 10

#ifdef LDR

int readLdr(void)
{
  uint32_t adc = 0;

  for (int i = 0 ; i < NSAMPLE ; i++) {
    adc += analogRead(LUMINOSITY_PIN);
    delay(10);
  }
  adc /= NSAMPLE;
  log_print("ADC: "); log_println(adc);
  float rVoltage = (adc * VREF / 1023);
  float ldrVoltage = VREF - rVoltage;
  float current = rVoltage / RREF;
  float resistance = ldrVoltage / current;
  log_print("VOLT: "); log_println(ldrVoltage);
  log_print("AMP: "); log_println(current, 5);
  float lightLevel = 0;
  lightLevel = (LUX_CALC_SCALAR * pow(resistance, LUX_CALC_EXPONENT));
  log_print("LUX: "); log_println(lightLevel, 5);
  return lightLevel;
}

int getLightLevel(void)
{
  static unsigned long lastTime;
  static float lastLevel = -1;
  float lightLevel = lastLevel;

  unsigned long now  = millis();
  unsigned long elapsed = now - lastTime;
  if (elapsed >= 10000) {
#ifdef LDR
    lightLevel = readLdr();
#endif

#ifdef MYSENSORS
    send(luminosityMsg.set(lightLevel, 2));
#endif
    if (lightLevel != lastLevel) {
      lastLevel = lightLevel;
    }
    lastTime = now;
  }
  return lightLevel;
}

#endif

static unsigned long lightStart;
static int lightDirection;

void checkRotaryEncoder()
{
  long encoderValue = knob.read();
  if (encoderValue > 100) {
    encoderValue = 100;
    knob.write(100);
  }
  else if (encoderValue < 0) {
    encoderValue = 0;
    knob.write(0);
  }
  if (encoderValue != dimmer.getBrightness()) {
    log_print("KNOB "); log_println(encoderValue);
    dimmer.setBrightness(encoderValue);
    dimmer.turnOnFixed(encoderValue);
  }
}

void checkButtonClick()
{
  static byte oldBtnState;

  debouncer.update();
  byte btnState = debouncer.read();
  if (btnState != oldBtnState && btnState == LOW) {
    log_println("BUTTON");
    long encoderValue = knob.read();
    changedByKnob = true;
    dimmer.setBrightness(encoderValue);
#ifdef MYSENSORS
    send(dimmerMsg.set(encoderValue), true);
    delay(1000);
    send(dimmerMsg.set(0), true);
#else
    saveLevelState(EEPROM_DIM_LEVEL, encoderValue);
#endif
    dimmer.turnOnFixed(0);
  }
  oldBtnState = btnState;
}

void turnOnTheLight(int direction)
{
  log_println("turn ON");
  lightDirection = direction;
  lightStart = millis();
  dimmer.turnOn(lightDirection);
}

void turnOffTheLight(int direction)
{
  log_println("turn OFF");
  dimmer.turnOff(direction);
  lightDirection = NONE;
}

void setup()
{
#ifndef MYSENSORS
  Serial.begin(115200);
  before();
#endif
  downDebouncer.attach(DOWN_PIR_PIN);
  downDebouncer.interval(5);
  debouncer.attach(KNOB_BUTTON_PIN);
  upDebouncer.attach(UP_PIR_PIN);
  upDebouncer.interval(5);
  debouncer.interval(5);
  knob.begin();
  // Retreive our last dim levels from the eprom
  byte brightness = loadLevelState(EEPROM_DIM_LEVEL);
  if (brightness == 0) {
    brightness = 50;
    saveLevelState(EEPROM_DIM_LEVEL, brightness);
  }
  log_println(brightness);
  knob.write(brightness);
  dimmer.setBrightness(brightness);
}

void before(void)
{
  log_println(SN);
  if (!io.begin(SX1509_ADDRESS)) {
    log_println("SX1509 not found");
  }
  for (int i = 0 ; i < _nLeds ; i++) {
    io.pinMode(leds[i].pin(), ANALOG_OUTPUT);
    io.pwm(leds[i].pin(), 250);
    delay(50);
  }
  for (int i = _nLeds - 1 ; i >= 0 ; i--) {
    io.pwm(leds[i].pin(), 255);
    delay(50);
  }
  pinMode(DOWN_PIR_PIN, INPUT);
  pinMode(UP_PIR_PIN, INPUT);
}

#ifdef MYSENSORS

void presentation()
{
  // Send the Sketch Version Information to the Gateway
  log_print("Presentation: ");
  present(CHILD_ID_LIGHT, S_DIMMER);
  present(CHILD_ID_MOTION_UP, S_MOTION);
  present(CHILD_ID_MOTION_DOWN, S_MOTION);
  present(CHILD_ID_LUMINOSITY, S_LIGHT_LEVEL);
  sendSketchInfo(SN, SV);
  log_println("OK");
}

void receive(const MyMessage &message)
{
  if (message.type == V_LIGHT) {
    // Incoming on/off command sent from controller ("1" or "0")
    int lightState = message.getString()[0] == '1';
    int newLevel = 0;
    log_print(lightState ? "ON" : "OFF");
    log_println(" received");
    if (lightState == HIGH) {
      // Pick up last saved dimmer level from the eeprom
      newLevel = loadLevelState(EEPROM_DIM_LEVEL);
      turnOnTheLight(UPSTAIRS);
      send(dimmerMsg.set(newLevel), true);
    }
    else {
      turnOffTheLight(UPSTAIRS);
      send(dimmerMsg.set(0), true);
    }
    // We do not change any levels here until ack comes back from gateway
    return;
  } else if (message.type == V_DIMMER) {
    // Incoming dim-level command sent from controller (or ack message)
    int brightness = atoi(message.getString(convBuffer));
    // Save received dim value to eeprom (unless turned off). Will be
    // retreived when a on command comes in
    if (brightness != 0) {
      log_print("New brightness received: ");
      log_println(brightness);
      dimmer.setBrightness(brightness);
      knob.write(brightness);
      saveLevelState(EEPROM_DIM_LEVEL, brightness);
      turnOnTheLight(UPSTAIRS);
    }
  }
  // Cancel send if user turns knob while message comes in
  sendDimValue = false;
}

#endif

void loop()
{
  float lightLevel = 0;

  checkRotaryEncoder();
  checkButtonClick();
  upDebouncer.update();
  downDebouncer.update();
#ifdef LDR
  // don't get light level if ligh is ON
  if (lightDirection == NONE) {
    lightLevel = getLightLevel();
  }
#endif
  if (downDebouncer.rose()) {
    log_println("DOWN");
    if (lightLevel > LIGHT_MAX_LEVEL) {
      return;
    }
    turnOnTheLight(DOWNSTAIRS);
#ifdef MYSENSORS
    send(dimmerMsg.set(dimmer.getBrightness()), true);
#endif
  }
  if (upDebouncer.rose()) {
    log_println("UP");
    if (lightLevel > LIGHT_MAX_LEVEL) {
      return;
    }
    turnOnTheLight(UPSTAIRS);
#ifdef MYSENSORS
    send(dimmerMsg.set(dimmer.getBrightness()), true);
#endif
  }
  if (lightDirection) {
    unsigned long now  = millis();
    unsigned long elapsed = now - lightStart;
    if (elapsed >= DIMMER_ON_TIME) {
      turnOffTheLight(lightDirection);
#ifdef MYSENSORS
      send(dimmerMsg.set(0), true);
#endif
    }
  }
  dimmer.step();
}

int loadLevelState(byte pos)
{
#ifdef MYSENSORS
  byte data = min(max(loadState(pos), 0), 100);
#else
  byte data = eeprom_read_byte((const uint8_t *)pos);
#endif
  log_print("reading: "); log_println(data);
  return data;
}

void saveLevelState(byte pos, byte data)
{
  log_print("saving: "); log_println(data);
#ifdef MYSENSORS
  saveState(pos, min(max(data, 0), 100));
#else
  eeprom_update_byte((uint8_t *)pos, (uint8_t)data);
#endif
}
