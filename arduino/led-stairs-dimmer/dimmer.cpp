
#include <Arduino.h>
#include <Wire.h>

#define DEBUG
#define LOG_DOMAIN    LOG_DIMMER
#include "debug.h"
#include "dimmer.h"

SX1509 io;

int DimmingLed::staticId = 0;

DimmingLed::DimmingLed(int pin)
{
  m_ledId = staticId++;
  m_ledPin = pin;
  m_endValue = 0;
  m_curValue = 0;
  m_step = 0;
  m_running = false;
}

int DimmingLed::pin(void)
{
  return m_ledPin;
}

int DimmingLed::value(void)
{
  return m_curValue;
}

int DimmingLed::getEndValue(void)
{
  return m_endValue;
}

void DimmingLed::setEndValue(int value)
{
  m_endValue = value;
  m_step = (m_endValue - m_curValue) < 0 ? -1 : 1;
}

void DimmingLed::tick(void)
{
  int changed = false;

  if (m_running) {
    if ((m_endValue > 0 && m_curValue < m_endValue) || (m_endValue == 0 && m_curValue > 0)) {
      m_curValue += m_step;
      changed = true;
    }
    io.pwm(m_ledPin, 255-(int)(m_curValue / 100.0 * 255));
    if (changed && m_curValue == m_endValue) {
      m_running = false;
    }
    log_print(micros()); log_print(" DimmingLed::tick["); log_print(m_ledId);
    log_print("] (pin"); log_print(m_ledPin); log_print("):"); log_print(m_curValue);
    log_print("/"); log_print(m_endValue); log_print(" ("); log_print(m_running); log_println(")");
  }
}

void DimmingLed::start(void)
{
#ifdef DEBUG
  log_print(micros());
  log_print(" DimmingLed::start[");
  log_print(m_ledId);
  log_println("] ");
#endif
  m_running  = true;
}

void DimmingLed::stop(void)
{
#ifdef DEBUG
  log_print(micros());
  log_print(" DimmingLed::stop[");
  log_print(m_ledId);
  log_println("] ");
#endif
  m_running  = false;
}

int DimmingLed::started(void)
{
  return m_running;
}

// leds: DimmingLed array
// n: number of LEDs
// Clock: dimmer Clock (ms)
// turnOnTrigger: % of brightness to begin to turn ON the next LED (0-100%)
// turnOffTrigger: % of brightness loss to begin to turn OFF the previous LED (0-100%)
// maximal brightness: (0-100%)

Dimmer::Dimmer(DimmingLed *leds, int n, int clock, int turnOnTrigger, int turnOffTrigger, int brightness)
{
  m_dimmingLeds = leds;
  m_nLeds = n;
  // m_dimmingBaseClock is the dimming clock at brightness 100%
  m_dimmingBaseClock = clock;
  // m_dimmingClock is the dimming clock for the given brightness
  m_dimmingClock = (long)m_dimmingBaseClock * (100.0 / brightness);
  m_onLedTrigger = min(turnOnTrigger, 100);
  m_offLedTrigger = min(turnOffTrigger, 100);
  m_maxBrightness = min(brightness, 100);
  m_lastStepTime = micros();
  m_direction = 0;
  m_action = 0;
  m_currentLed = 0;
}

void Dimmer::setBrightness(int brightness)
{
  m_maxBrightness = min(brightness, 100);
  m_dimmingClock = (long)m_dimmingBaseClock * (100.0 / m_maxBrightness);
#ifdef DEBUG
  log_print("setBrightness ");
  log_print(m_maxBrightness);
  log_print(" m_dimmingClock ");
  log_println(m_dimmingClock);
#endif
}

void Dimmer::turnOn(int direction)
{
  m_action = INCREASE;
  m_direction = direction;
  m_currentLed = direction == UPSTAIRS ? 0 : m_nLeds - 1;
  for (int i = 0 ; i < m_nLeds ; i++) {
    m_dimmingLeds[i].setEndValue(m_maxBrightness);
    m_dimmingLeds[i].stop();
  }
  log_print("turnOn "); log_print(m_currentLed); log_print(" m_direction "); log_println(m_direction);
  m_dimmingLeds[m_currentLed].start();
}

void Dimmer::turnOff(int direction)
{
  m_action = DECREASE;
  m_direction = direction;
  m_currentLed = direction == UPSTAIRS ? 0 : m_nLeds - 1;
  for (int i = 0 ; i < m_nLeds ; i++) {
    m_dimmingLeds[i].setEndValue(0);
    m_dimmingLeds[i].stop();
  }
  log_print("turnOff "); log_print(m_currentLed); log_print(" m_direction "); log_println(m_direction);
  m_dimmingLeds[m_currentLed].start();
}

void Dimmer::turnOnFixed(int brightness)
{
  log_print("turnTo ");
  log_println(brightness);
  for (int i = 0 ; i < m_nLeds ; i++) {
    io.pwm(m_dimmingLeds[i].pin(), 255-(int)(brightness / 100.0 * 255));
  }
}

void Dimmer::step()
{
  static unsigned long _lastStepTime;
  DimmingLed *pLed = &m_dimmingLeds[m_currentLed];
  unsigned long currentTime  = micros();
  if (currentTime - m_lastStepTime > m_dimmingClock) {
    m_lastStepTime = currentTime;
    pLed->tick();
    if (pLed->started()) {
      if (m_action == INCREASE) {
        if (pLed->value() >= (pLed->getEndValue() * m_onLedTrigger / 100)) {
          if (m_direction == UPSTAIRS) {
            if (m_currentLed < m_nLeds - 1) {
              pLed++;
              if (!pLed->started()) {
                pLed->start();
              }
            }
          }
          else {
            if (m_currentLed > 0) {
              pLed--;
              if (!pLed->started()) {
                pLed->start();
              }
            }
          }
        }
      }
      else if (m_action == DECREASE) {
        if (pLed->value() <= (m_maxBrightness * (100 - m_offLedTrigger) / 100)) {
          if (m_direction == UPSTAIRS) {
            if (m_currentLed < m_nLeds - 1) {
              pLed++;
              if (!pLed->started()) {
                pLed->start();
              }
            }
          }
          else {
            if (m_currentLed > 0) {
              pLed--;
              if (!pLed->started()) {
                pLed->start();
              }
            }
          }
        }
      }
    }
    _nextLed();
  }
}

void Dimmer::_nextLed(void)
{
  if (m_direction == UPSTAIRS) {
    m_currentLed = m_currentLed == m_nLeds - 1 ? 0 : m_currentLed + 1;
  }
  else {
    m_currentLed = m_currentLed == 0 ? m_nLeds - 1 : m_currentLed - 1;
  }
}

void Dimmer::setClock(int clock)
{
  m_dimmingClock = clock;
}

bool Dimmer::isIdle(void)
{
  for (int i = 0 ; i < m_nLeds ; i++) {
    if (m_dimmingLeds[i].started()) {
      return false;
    }
  }
  return true;
}
