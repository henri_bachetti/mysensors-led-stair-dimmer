
#include <Arduino.h>
#include <Bounce2.h>

#define LOG_DOMAIN    LOG_ENCODER
#include "debug.h"
#include "encoder.h"

volatile byte aFlag = 0;
volatile byte bFlag = 0;
volatile long encoderPos = 0;
volatile byte reading = 0;
int step;

static Bounce buttonDebouncer = Bounce();

void PinA() {
  cli();
  reading = PIND & 0xC;
  if (reading == B00001100 && aFlag) {
    encoderPos -= step;
    bFlag = 0;
    aFlag = 0;
  }
  else if (reading == B00000100) bFlag = 1;
  sei();
}

void PinB() {
  cli();
  reading = PIND & 0xC;
  if (reading == B00001100 && bFlag) {
    encoderPos += step;
    bFlag = 0;
    aFlag = 0;
  }
  else if (reading == B00001000) aFlag = 1;
  sei();
}
Encoder::Encoder(uint8_t pinA, uint8_t pinB, uint8_t button) :
  m_button(button)
{
  step = 1;
  pinMode(pinA, INPUT_PULLUP);
  pinMode(pinB, INPUT_PULLUP);
  attachInterrupt(0, PinA, RISING);
  attachInterrupt(1, PinB, RISING);
}

void Encoder::begin(void)
{
  pinMode(m_button, INPUT_PULLUP);
  buttonDebouncer.attach(m_button);
  buttonDebouncer.interval(5);
}

void Encoder::setStep(int s)
{
  step = s;
}

long Encoder::read(void)
{
  static long old;
  if (old != encoderPos) {
    log_print("encoder::read="); log_println(encoderPos);
    old = encoderPos;
  }
  return encoderPos;
}

uint8_t Encoder::readButton(void)
{
  static byte oldButtonVal = HIGH;
  buttonDebouncer.update();
  byte buttonVal = buttonDebouncer.read();
  if (oldButtonVal != buttonVal) {
    log_print(F("BUTTON: "));
    log_println(buttonVal);
    oldButtonVal = buttonVal;
    return buttonVal;
  }
  return -1;
}

void Encoder::write(long p)
{
  encoderPos = p;
  log_print("encoder::write="); log_println(encoderPos);
}
