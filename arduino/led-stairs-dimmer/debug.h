
#ifndef _DEBUG_H_
#define _DEBUG_H_

#define LOG_MAIN        0x01
#define LOG_DIMMER      0x02
#define LOG_ENCODER     0x04

#define LOGS_ACTIVE     (LOG_MAIN)

#if (LOGS_ACTIVE & LOG_DOMAIN)
#define log_print       Serial.print
#define log_println     Serial.println
#define log_printf      Serial.printf
#else
#define log_print
#define log_println
#define log_printf
#endif

#endif
