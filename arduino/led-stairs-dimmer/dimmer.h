#ifndef __DIMMER_H__
#define __DIMMER_H__

//#define DEBUG

#include <SparkFunSX1509.h>

#define NONE                  0
#define UPSTAIRS              1
#define DOWNSTAIRS            2
#define INCREASE              1
#define DECREASE              2

class DimmingLed
{
    static int staticId;
  private:
    int m_ledId;
    int m_ledPin;
    int m_endValue;
    int m_curValue;
    int m_step;
    int m_running;

  public:
    DimmingLed(int pin);
    int pin(void);
    int value(void);
    int getEndValue(void);
    void setEndValue(int value);
    void tick(void);
    void start(void);
    void stop(void);
    int started(void);
};

class Dimmer
{
  private:
    DimmingLed *m_dimmingLeds;
    int m_nLeds;
    int m_dimmingBaseClock;
    long m_dimmingClock;
    int m_onLedTrigger;
    int m_offLedTrigger;
    unsigned long m_lastStepTime;
    int m_direction;
    int m_action;
    int m_maxBrightness;
    int m_currentLed;
    void _nextLed(void);

  public:
    Dimmer(DimmingLed *leds, int n, int clock, int turnOnTrigger, int turnOffTrigger, int brightness);
    void setBrightness(int brightness);
    int getBrightness() {return m_maxBrightness;}
    int getDirection() {return m_direction;}
    void turnOn(int direction);
    void turnOff(int direction);
    void turnOnFixed(int brightness);
    void setClock(int clock);
    void step();
    bool isIdle(void);
};

extern SX1509 io;

#endif
