EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:my-microcontrollers
LIBS:my-power-supplies
LIBS:my-passives
LIBS:my-regulators
LIBS:my-misc-modules
LIBS:my-switches
LIBS:my-rotary-encoders
LIBS:led-stairs-dimmer-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X03 P1
U 1 1 5C7F240A
P 2250 2750
F 0 "P1" H 2250 2950 50  0000 C CNN
F 1 "PIR-DOWN" V 2350 2750 50  0000 C CNN
F 2 "myConnectors:NINIGI_NS25-W3K" H 2250 2750 50  0001 C CNN
F 3 "" H 2250 2750 50  0000 C CNN
	1    2250 2750
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X03 P2
U 1 1 5C7F26D6
P 2250 3250
F 0 "P2" H 2250 3450 50  0000 C CNN
F 1 "PIR-UP" V 2350 3250 50  0000 C CNN
F 2 "myConnectors:NINIGI_NS25-W3K" H 2250 3250 50  0001 C CNN
F 3 "" H 2250 3250 50  0000 C CNN
	1    2250 3250
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR2
U 1 1 5C7F28B8
P 2550 3450
F 0 "#PWR2" H 2550 3200 50  0001 C CNN
F 1 "GND" H 2550 3300 50  0000 C CNN
F 2 "" H 2550 3450 50  0000 C CNN
F 3 "" H 2550 3450 50  0000 C CNN
	1    2550 3450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR8
U 1 1 5C7F63C1
P 3600 6650
F 0 "#PWR8" H 3600 6400 50  0001 C CNN
F 1 "GND" H 3600 6500 50  0000 C CNN
F 2 "" H 3600 6650 50  0000 C CNN
F 3 "" H 3600 6650 50  0000 C CNN
	1    3600 6650
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P3
U 1 1 5C804795
P 3300 6450
F 0 "P3" H 3300 6600 50  0000 C CNN
F 1 "12V" V 3400 6450 50  0000 C CNN
F 2 "myConnectors:JST-B2P-VH" H 3300 6450 50  0001 C CNN
F 3 "" H 3300 6450 50  0000 C CNN
	1    3300 6450
	-1   0    0    1   
$EndComp
$Comp
L C C4
U 1 1 5C81404C
P 5650 2400
F 0 "C4" H 5675 2500 50  0000 L CNN
F 1 "100nF" H 5400 2300 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 5688 2250 50  0001 C CNN
F 3 "" H 5650 2400 50  0000 C CNN
	1    5650 2400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR17
U 1 1 5C81427D
P 5650 2650
F 0 "#PWR17" H 5650 2400 50  0001 C CNN
F 1 "GND" H 5650 2500 50  0000 C CNN
F 2 "" H 5650 2650 50  0000 C CNN
F 3 "" H 5650 2650 50  0000 C CNN
	1    5650 2650
	1    0    0    -1  
$EndComp
$Comp
L NRF24L01 U1
U 1 1 5CE2D28A
P 4200 5050
F 0 "U1" H 4050 5300 60  0000 C CNN
F 1 "NRF24L01" H 4500 5500 60  0000 C CNN
F 2 "myModules:NRF24L01" H 4200 5050 60  0001 C CNN
F 3 "" H 4200 5050 60  0000 C CNN
	1    4200 5050
	1    0    0    -1  
$EndComp
$Comp
L +12V #PWR9
U 1 1 5D407AAB
P 3700 5800
F 0 "#PWR9" H 3700 5650 50  0001 C CNN
F 1 "+12V" H 3700 5940 50  0000 C CNN
F 2 "" H 3700 5800 50  0000 C CNN
F 3 "" H 3700 5800 50  0000 C CNN
	1    3700 5800
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR13
U 1 1 5D422C5E
P 4900 6300
F 0 "#PWR13" H 4900 6150 50  0001 C CNN
F 1 "+3.3V" H 4900 6440 50  0000 C CNN
F 2 "" H 4900 6300 50  0000 C CNN
F 3 "" H 4900 6300 50  0000 C CNN
	1    4900 6300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR10
U 1 1 5D42365E
P 3900 6900
F 0 "#PWR10" H 3900 6650 50  0001 C CNN
F 1 "GND" H 3900 6750 50  0000 C CNN
F 2 "" H 3900 6900 50  0000 C CNN
F 3 "" H 3900 6900 50  0000 C CNN
	1    3900 6900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR14
U 1 1 5D42400E
P 4900 6900
F 0 "#PWR14" H 4900 6650 50  0001 C CNN
F 1 "GND" H 4900 6750 50  0000 C CNN
F 2 "" H 4900 6900 50  0000 C CNN
F 3 "" H 4900 6900 50  0000 C CNN
	1    4900 6900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR12
U 1 1 5D424239
P 4400 6800
F 0 "#PWR12" H 4400 6550 50  0001 C CNN
F 1 "GND" H 4400 6650 50  0000 C CNN
F 2 "" H 4400 6800 50  0000 C CNN
F 3 "" H 4400 6800 50  0000 C CNN
	1    4400 6800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR21
U 1 1 5E4D66CE
P 6850 3600
F 0 "#PWR21" H 6850 3350 50  0001 C CNN
F 1 "GND" H 6850 3450 50  0000 C CNN
F 2 "" H 6850 3600 50  0000 C CNN
F 3 "" H 6850 3600 50  0000 C CNN
	1    6850 3600
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR20
U 1 1 5E4D6D76
P 6750 900
F 0 "#PWR20" H 6750 750 50  0001 C CNN
F 1 "+3.3V" H 6750 1040 50  0000 C CNN
F 2 "" H 6750 900 50  0000 C CNN
F 3 "" H 6750 900 50  0000 C CNN
	1    6750 900 
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR6
U 1 1 5E4D70A6
P 3400 3850
F 0 "#PWR6" H 3400 3700 50  0001 C CNN
F 1 "+3.3V" H 3400 3990 50  0000 C CNN
F 2 "" H 3400 3850 50  0000 C CNN
F 3 "" H 3400 3850 50  0000 C CNN
	1    3400 3850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR11
U 1 1 5E4D7399
P 4200 5700
F 0 "#PWR11" H 4200 5450 50  0001 C CNN
F 1 "GND" H 4200 5550 50  0000 C CNN
F 2 "" H 4200 5700 50  0000 C CNN
F 3 "" H 4200 5700 50  0000 C CNN
	1    4200 5700
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR16
U 1 1 5E4D787C
P 5650 2050
F 0 "#PWR16" H 5650 1900 50  0001 C CNN
F 1 "+3.3V" H 5650 2190 50  0000 C CNN
F 2 "" H 5650 2050 50  0000 C CNN
F 3 "" H 5650 2050 50  0000 C CNN
	1    5650 2050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR15
U 1 1 5E4D7F5E
P 5400 1950
F 0 "#PWR15" H 5400 1700 50  0001 C CNN
F 1 "GND" H 5400 1800 50  0000 C CNN
F 2 "" H 5400 1950 50  0000 C CNN
F 3 "" H 5400 1950 50  0000 C CNN
	1    5400 1950
	1    0    0    -1  
$EndComp
$Comp
L arduino_mini_8MHz U2
U 1 1 5E4D83D0
P 4250 2450
F 0 "U2" H 4250 3000 70  0000 C CNN
F 1 "arduino_mini_8MHz" H 4225 2825 70  0000 C CNN
F 2 "myMicroControllers:Arduino-mini" H 4250 2000 60  0001 C CNN
F 3 "" H 4550 1950 60  0000 C CNN
	1    4250 2450
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 5E4DDEB5
P 3400 4650
F 0 "C1" H 3425 4750 50  0000 L CNN
F 1 "100nF" H 3150 4550 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 3438 4500 50  0001 C CNN
F 3 "" H 3400 4650 50  0000 C CNN
	1    3400 4650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR7
U 1 1 5E4DE2AE
P 3400 5050
F 0 "#PWR7" H 3400 4800 50  0001 C CNN
F 1 "GND" H 3400 4900 50  0000 C CNN
F 2 "" H 3400 5050 50  0000 C CNN
F 3 "" H 3400 5050 50  0000 C CNN
	1    3400 5050
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P20
U 1 1 5E4E8B10
P 2250 4900
F 0 "P20" H 2250 5050 50  0000 C CNN
F 1 "LDR" V 2350 4900 50  0000 C CNN
F 2 "myConnectors:NINIGI_NS25-W2K" H 2250 4900 50  0001 C CNN
F 3 "" H 2250 4900 50  0000 C CNN
	1    2250 4900
	-1   0    0    -1  
$EndComp
$Comp
L CP C5
U 1 1 5E4E9619
P 5900 4200
F 0 "C5" H 5925 4300 50  0000 L CNN
F 1 "220µF" H 5925 4100 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D5_L11_P2.5" H 5938 4050 50  0001 C CNN
F 3 "" H 5900 4200 50  0000 C CNN
	1    5900 4200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR19
U 1 1 5E4E9B3F
P 5900 4550
F 0 "#PWR19" H 5900 4300 50  0001 C CNN
F 1 "GND" H 5900 4400 50  0000 C CNN
F 2 "" H 5900 4550 50  0000 C CNN
F 3 "" H 5900 4550 50  0000 C CNN
	1    5900 4550
	1    0    0    -1  
$EndComp
$Comp
L R R33
U 1 1 5E4E9DDA
P 5700 4200
F 0 "R33" V 5780 4200 50  0000 C CNN
F 1 "10K" V 5700 4200 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 5630 4200 50  0001 C CNN
F 3 "" H 5700 4200 50  0000 C CNN
	1    5700 4200
	-1   0    0    1   
$EndComp
$Comp
L +3.3V #PWR4
U 1 1 5E4EB328
P 2600 2550
F 0 "#PWR4" H 2600 2400 50  0001 C CNN
F 1 "+3.3V" H 2600 2690 50  0000 C CNN
F 2 "" H 2600 2550 50  0000 C CNN
F 3 "" H 2600 2550 50  0000 C CNN
	1    2600 2550
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR3
U 1 1 5E4EB41D
P 2550 4750
F 0 "#PWR3" H 2550 4600 50  0001 C CNN
F 1 "+3.3V" H 2550 4890 50  0000 C CNN
F 2 "" H 2550 4750 50  0000 C CNN
F 3 "" H 2550 4750 50  0000 C CNN
	1    2550 4750
	1    0    0    -1  
$EndComp
$Comp
L Rotary_Encoder_Switch SW1
U 1 1 5E500BC9
P 2350 1600
F 0 "SW1" H 2350 1860 50  0000 C CNN
F 1 "ALPS-EC11E" H 2350 1950 50  0000 C CNN
F 2 "mySwitches:RotaryEncoder_Alps_EC11E-Switch_Vertical_H20mm" H 2250 1760 50  0001 C CNN
F 3 "" H 2350 1860 50  0001 C CNN
	1    2350 1600
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR5
U 1 1 5E501298
P 2750 1800
F 0 "#PWR5" H 2750 1550 50  0001 C CNN
F 1 "GND" H 2750 1650 50  0000 C CNN
F 2 "" H 2750 1800 50  0000 C CNN
F 3 "" H 2750 1800 50  0000 C CNN
	1    2750 1800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR1
U 1 1 5E501510
P 1950 1800
F 0 "#PWR1" H 1950 1550 50  0001 C CNN
F 1 "GND" H 1950 1650 50  0000 C CNN
F 2 "" H 1950 1800 50  0000 C CNN
F 3 "" H 1950 1800 50  0000 C CNN
	1    1950 1800
	1    0    0    -1  
$EndComp
$Comp
L C C6
U 1 1 5E5281DA
P 5750 1250
F 0 "C6" H 5775 1350 50  0000 L CNN
F 1 "100nF" H 5500 1150 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 5788 1100 50  0001 C CNN
F 3 "" H 5750 1250 50  0000 C CNN
	1    5750 1250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR18
U 1 1 5E5284CC
P 5750 1500
F 0 "#PWR18" H 5750 1250 50  0001 C CNN
F 1 "GND" H 5750 1350 50  0000 C CNN
F 2 "" H 5750 1500 50  0000 C CNN
F 3 "" H 5750 1500 50  0000 C CNN
	1    5750 1500
	1    0    0    -1  
$EndComp
$Comp
L LM78L33 U3
U 1 1 5EE6023B
P 4400 6450
F 0 "U3" H 4550 6400 50  0000 C CNN
F 1 "LM78L33" H 4400 6650 50  0000 C CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 4400 6550 50  0001 C CIN
F 3 "" H 4400 6450 50  0000 C CNN
	1    4400 6450
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 5EE60936
P 3900 6650
F 0 "C2" H 3925 6750 50  0000 L CNN
F 1 "470nF" H 3950 6550 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 3938 6500 50  0001 C CNN
F 3 "" H 3900 6650 50  0000 C CNN
	1    3900 6650
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 5EE60E04
P 4900 6650
F 0 "C3" H 4925 6750 50  0000 L CNN
F 1 "100nF" H 4650 6550 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 4938 6500 50  0001 C CNN
F 3 "" H 4900 6650 50  0000 C CNN
	1    4900 6650
	1    0    0    -1  
$EndComp
$Comp
L SX-1509-MODULE U4
U 1 1 5E4D50B7
P 6150 1700
F 0 "U4" H 6250 1125 60  0000 C CNN
F 1 "SX-1509-MODULE" H 6625 1025 60  0000 C CNN
F 2 "myModules:SX-1509-MODULE" H 5950 1700 60  0001 C CNN
F 3 "" H 5950 1700 60  0000 C CNN
	1    6150 1700
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X17 P4
U 1 1 5EE61766
P 8750 2450
F 0 "P4" H 8750 3350 50  0000 C CNN
F 1 "OUT" V 8850 2450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x17" H 8750 2450 50  0001 C CNN
F 3 "" H 8750 2450 50  0000 C CNN
	1    8750 2450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR22
U 1 1 5EE6307A
P 8450 3300
F 0 "#PWR22" H 8450 3050 50  0001 C CNN
F 1 "GND" H 8450 3150 50  0000 C CNN
F 2 "" H 8450 3300 50  0000 C CNN
F 3 "" H 8450 3300 50  0000 C CNN
	1    8450 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 2850 2550 3450
Wire Wire Line
	2550 2850 2450 2850
Wire Wire Line
	2450 3350 2550 3350
Connection ~ 2550 3350
Wire Wire Line
	2600 2550 2600 3150
Wire Wire Line
	2600 2650 2450 2650
Connection ~ 2600 2650
Wire Wire Line
	5650 2050 5650 2250
Wire Wire Line
	3500 6400 4000 6400
Wire Wire Line
	3600 6000 3600 6650
Connection ~ 5650 2150
Wire Wire Line
	3600 6500 3500 6500
Wire Wire Line
	3700 5800 3700 6400
Connection ~ 3700 6400
Wire Wire Line
	2600 3150 2450 3150
Wire Wire Line
	3900 6900 3900 6800
Wire Wire Line
	4900 6300 4900 6500
Wire Wire Line
	4900 6400 4800 6400
Connection ~ 4900 6400
Wire Wire Line
	4900 6900 4900 6800
Wire Wire Line
	4400 6800 4400 6700
Wire Wire Line
	5950 1800 5800 1800
Wire Wire Line
	5800 1800 5800 3850
Wire Wire Line
	5800 3850 4150 3850
Wire Wire Line
	4150 3850 4150 3750
Wire Wire Line
	4000 3750 4000 3900
Wire Wire Line
	4000 3900 5850 3900
Wire Wire Line
	5850 1900 5950 1900
Wire Wire Line
	6850 3600 6750 3500
Wire Wire Line
	6750 900  6750 1400
Wire Wire Line
	4200 4250 4200 4500
Wire Wire Line
	4200 5700 4200 5600
Wire Wire Line
	5250 2150 5650 2150
Wire Wire Line
	5650 2650 5650 2550
Wire Wire Line
	5400 1950 5400 1850
Wire Wire Line
	5400 1850 5250 1850
Wire Wire Line
	2450 2750 3200 2750
Wire Wire Line
	2450 3250 2650 3250
Wire Wire Line
	2650 3250 2650 2900
Wire Wire Line
	2650 2900 3200 2900
Wire Wire Line
	4650 5050 5450 5050
Wire Wire Line
	5450 5050 5450 3200
Wire Wire Line
	5450 3200 5250 3200
Wire Wire Line
	3750 5150 3650 5150
Wire Wire Line
	3650 5150 3650 4450
Wire Wire Line
	3650 4450 5400 4450
Wire Wire Line
	5400 4450 5400 3050
Wire Wire Line
	5400 3050 5250 3050
Wire Wire Line
	3750 5050 3600 5050
Wire Wire Line
	3600 5050 3600 4350
Wire Wire Line
	3600 4350 5500 4350
Wire Wire Line
	5500 4350 5500 2900
Wire Wire Line
	5500 2900 5250 2900
Wire Wire Line
	3200 3050 3100 3050
Wire Wire Line
	3100 3050 3100 4950
Wire Wire Line
	3100 4950 3750 4950
Wire Wire Line
	4650 4950 4750 4950
Wire Wire Line
	4750 4950 4750 4400
Wire Wire Line
	4750 4400 3050 4400
Wire Wire Line
	3050 4400 3050 3200
Wire Wire Line
	3050 3200 3200 3200
Wire Wire Line
	3900 6500 3900 6400
Connection ~ 3900 6400
Wire Wire Line
	4200 4250 3400 4250
Wire Wire Line
	3400 3850 3400 4500
Wire Wire Line
	3400 5050 3400 4800
Connection ~ 3400 4250
Wire Wire Line
	2750 3950 5900 3950
Wire Wire Line
	5350 3950 5350 2750
Wire Wire Line
	5900 4350 5900 4550
Wire Wire Line
	5700 4350 5700 4450
Wire Wire Line
	5700 4450 5900 4450
Connection ~ 5900 4450
Wire Wire Line
	5850 3900 5850 1900
Wire Wire Line
	5900 3950 5900 4050
Connection ~ 5350 3950
Wire Wire Line
	5700 4050 5700 3950
Connection ~ 5700 3950
Wire Wire Line
	5350 2750 5250 2750
Wire Wire Line
	2550 4750 2550 4850
Wire Wire Line
	2550 4850 2450 4850
Wire Wire Line
	2750 3950 2750 4950
Wire Wire Line
	2750 4950 2450 4950
Wire Wire Line
	2650 1600 2750 1600
Wire Wire Line
	2750 1600 2750 1800
Wire Wire Line
	2050 1700 1950 1700
Wire Wire Line
	1950 1700 1950 1800
Wire Wire Line
	2650 1700 3000 1700
Wire Wire Line
	3000 1700 3000 2300
Wire Wire Line
	3000 2300 3200 2300
Wire Wire Line
	3200 2450 3050 2450
Wire Wire Line
	3050 2450 3050 1500
Wire Wire Line
	3050 1500 2650 1500
Wire Wire Line
	2050 1500 1950 1500
Wire Wire Line
	1950 1500 1950 1200
Wire Wire Line
	1950 1200 3100 1200
Wire Wire Line
	3100 1200 3100 2600
Wire Wire Line
	3100 2600 3200 2600
Wire Wire Line
	5750 1400 5750 1500
Wire Wire Line
	6750 1000 5750 1000
Wire Wire Line
	5750 1000 5750 1100
Connection ~ 6750 1000
Wire Wire Line
	8450 3300 8450 1650
Wire Wire Line
	8450 1650 8550 1650
Wire Wire Line
	7550 2500 7650 2500
Wire Wire Line
	7650 2500 7650 1750
Wire Wire Line
	7650 1750 8550 1750
Wire Wire Line
	7550 2600 7700 2600
Wire Wire Line
	7700 2600 7700 1850
Wire Wire Line
	7700 1850 8550 1850
Wire Wire Line
	7550 2700 7750 2700
Wire Wire Line
	7750 2700 7750 1950
Wire Wire Line
	7750 1950 8550 1950
Wire Wire Line
	8550 2050 7800 2050
Wire Wire Line
	7800 2050 7800 2800
Wire Wire Line
	7800 2800 7550 2800
Wire Wire Line
	7550 2900 7850 2900
Wire Wire Line
	7850 2900 7850 2150
Wire Wire Line
	7850 2150 8550 2150
Wire Wire Line
	8550 2250 7900 2250
Wire Wire Line
	7900 2250 7900 3000
Wire Wire Line
	7900 3000 7550 3000
Wire Wire Line
	7550 3100 7950 3100
Wire Wire Line
	7950 3100 7950 2350
Wire Wire Line
	7950 2350 8550 2350
Wire Wire Line
	8550 2450 8000 2450
Wire Wire Line
	8000 2450 8000 3200
Wire Wire Line
	8000 3200 7550 3200
Wire Wire Line
	7550 1700 8400 1700
Wire Wire Line
	8400 1700 8400 2550
Wire Wire Line
	8400 2550 8550 2550
Wire Wire Line
	7550 1800 8350 1800
Wire Wire Line
	8350 1800 8350 2650
Wire Wire Line
	8350 2650 8550 2650
Wire Wire Line
	7550 1900 8300 1900
Wire Wire Line
	8300 1900 8300 2750
Wire Wire Line
	8300 2750 8550 2750
Wire Wire Line
	7550 2000 8250 2000
Wire Wire Line
	8250 2000 8250 2850
Wire Wire Line
	8250 2850 8550 2850
Wire Wire Line
	7550 2100 8200 2100
Wire Wire Line
	8200 2100 8200 2950
Wire Wire Line
	8200 2950 8550 2950
Wire Wire Line
	7550 2200 8150 2200
Wire Wire Line
	8150 2200 8150 3050
Wire Wire Line
	8150 3050 8550 3050
Wire Wire Line
	7550 2300 8100 2300
Wire Wire Line
	8100 2300 8100 3150
Wire Wire Line
	8100 3150 8550 3150
Wire Wire Line
	7550 2400 8050 2400
Wire Wire Line
	8050 2400 8050 3250
Wire Wire Line
	8050 3250 8550 3250
$Comp
L CONN_01X02 P5
U 1 1 5EE61B9C
P 3300 5950
F 0 "P5" H 3300 6100 50  0000 C CNN
F 1 "12V" V 3400 5950 50  0000 C CNN
F 2 "myConnectors:JST-B2P-VH" H 3300 5950 50  0001 C CNN
F 3 "" H 3300 5950 50  0000 C CNN
	1    3300 5950
	-1   0    0    1   
$EndComp
Wire Wire Line
	3700 5900 3500 5900
Connection ~ 3700 5900
Wire Wire Line
	3500 6000 3600 6000
Connection ~ 3600 6500
$EndSCHEMATC
