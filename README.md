# LED STAIRS LIGHTING

The purpose of this page is to explain step by step the realization of a LED stairs lighting system based on ARDUINO PRO MINI 8MHz.

It can be optionally connected by radio to a DOMOTICZ server, using an NRF24L01 2.4GHZ module.

It includes a dimmer for progressive lighting.

The board uses the following components :

 * an ARDUINO PRO MINI 8MHz 3.3V
 * a SX1509 module
 * a NRF24L01 module (optional)
 * 2 HC-SR501 motion sensor
 * 16 AO3400 MOSFETS
 * a LM78L33 regulator
 * a rotary encoder
 * a LDR (optional)
 * some passive components
 * the board is powered by a 12V switching power supply.

The radio module is optional :

To enable it, just uncomment this line

//#define MYSENSORS

The LDR is optional :

To disable it, just comment this line

//#define LDR

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.11.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2020/08/un-eclairage-descalier-leds-nouvelle.html

